<?php

namespace Vinsto\Core\RemoteService\Manager;

use Npaf\Authentication\AccessTokenMetadata;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\Client;
use Vinsto\Lib\Base\RemoteServiceBase;

class RemoteServiceManager
{
    /**
     * @var array
     */
    private $infoCenterConfig;

    private $infoCenterParsingRemoteServiceApi;

    private $parsingPodServiceTypeApi;

    public function __construct(array $infoCenterConfig)
    {
        $this->infoCenterConfig = $infoCenterConfig;
        $this->infoCenterURL =
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Protocal'] . '://' .
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Host'] . ':' .
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__Port'];
//        $this->infoCenterReceptionApi =
//            $this->config['global']['INFO_CENTER_a681207__Reception_api'];
        $this->infoCenterParsingRemoteServiceApi =
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__parsing_Remote_service_Api'];
//        $this->infoCenterHandlingPodTypeApi =
//            $this->config['global']['INFO_CENTER_a681207__PodType_api'];
//        $this->parsingPodTypeIdApi =
//            $this->config['global']['INFO_CENTER_a681207__parsing_PodType_Id_Api'];
        $this->parsingPodServiceTypeApi =
            $this->infoCenterConfig['global']['INFO_CENTER_a681207__parsing_PodService_Type_Api'];
//        $this->podInfoReporterPrjName =
//            $this->config['global']['POD_INFO_REPORTER_c41a5d9__Prj_name'];
    }


    public function parse(string $serviceName, RequestStack $requestStack): string
    {
        /**
         *
         * Todo: 关于PodService的两次API调用 ::: getServiceType() 和 $serviceUrl ::: 变为一次
         *     可能的解决方案： InfoCenter中新建API将PodService串行化，并将JSON传递过来
         *
         */

        $request = $requestStack->getCurrentRequest();
        $postData = json_decode( ($request->request->all())['json'], true );

        $serviceType = $this->getServiceType($serviceName);
        $newPostData = $this->lowercaseKeys($postData);
        $ls_service_type = strtolower($serviceType);
        if ($ls_service_type === 'url') {
            $operation = RemoteServiceBase::LABEL_GENERAL_OPERATION___GET_URL;
        }
        elseif (empty($newPostData[$ls_service_type])) {
            throw new \Exception("Error: query configuration not correct!", 1609529635);
        }else{
            $config = $newPostData[$ls_service_type];
            if (empty($config[RemoteServiceBase::TEXT__OPERATION])) {
                throw new \Exception("Error: '" . RemoteServiceBase::TEXT__OPERATION .  "'' in configuration NOT found!", 1609529635);
            }

            $operation = $config[RemoteServiceBase::TEXT__OPERATION];
        }

        /**
         * Todo: 使用SOLID/Design Patterns，不需要修改代码，自动增加类型
         */
        switch ($operation)
        {
            case RemoteServiceBase::LABEL_GENERAL_OPERATION___GET_URL:
                $reqUrl = $this->infoCenterURL . $this->infoCenterParsingRemoteServiceApi;
                $serviceUrl = `curl $reqUrl/$serviceName`;

                return $serviceUrl;
                break;

            case RemoteServiceBase::LABEL_GENERAL_OPERATION___GET:
                $reqUrl = $this->infoCenterURL . $this->infoCenterParsingRemoteServiceApi;
                $serviceUrl = `curl $reqUrl/$serviceName`;
                $client = new Client();
                $response = $client->get($serviceUrl);
                return (string) $response->getBody();
                break;

            case RemoteServiceBase::LABEL_ENDPOINT_OPERATION___GET_RESOURCE_LISTING:
                $reqUrl = $this->infoCenterURL . $this->infoCenterParsingRemoteServiceApi;
                $serviceUrl = `curl $reqUrl/$serviceName`;
                $client = new Client();
                $response = $client->get($serviceUrl);
                return (string) $response->getBody();
                break;

            case RemoteServiceBase::LABEL_GENERAL_OPERATION___POST:
                $reqUrl = $this->infoCenterURL . $this->infoCenterParsingRemoteServiceApi;
                $serviceUrl = `curl $reqUrl/$serviceName`;

                $client = new Client();
                $response = $client->post($serviceUrl, [
                    'form_params' => $newPostData[$ls_service_type][RemoteServiceBase::TEXT__DATA]
                ]);
                return (string) $response->getBody();
                break;

            case RemoteServiceBase::LABEL_ENDPOINT_OPERATION___GET_RESOURCE_DETAIL:
                $reqUrl = $this->infoCenterURL . $this->infoCenterParsingRemoteServiceApi;
                $serviceUrl = `curl $reqUrl/$serviceName`;

                $client = new Client();
                $id = $newPostData[$ls_service_type][RemoteServiceBase::TEXT__DATA]['id'];
                $response = $client->get("$serviceUrl/$id");
                return (string) $response->getBody();
                break;

            default:
                break;
        }

        return new JsonResponse([]);
    }


    protected function getServiceType(string $serviceName): string
    {
        $reqUrl = $this->infoCenterURL . $this->parsingPodServiceTypeApi;
        $serviceType = `curl $reqUrl/$serviceName`;

        return $serviceType;
    }

    protected function lowercaseKeys(array $postData): array
    {
        $res = [];
        foreach ($postData as $k => $v) {
            $res[strtolower($k)] = $v;
        }

        return $res;
    }

}
